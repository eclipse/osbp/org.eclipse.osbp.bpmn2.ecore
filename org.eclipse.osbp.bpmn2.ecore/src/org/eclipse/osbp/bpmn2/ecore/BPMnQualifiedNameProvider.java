/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     Jan Koehnlein - Initial contribution and API
 *     Christophe Loetz (Loetz GmbH&Co.KG) - integration in and adaption to OSBP project                                                       
 *******************************************************************************/

package org.eclipse.osbp.bpmn2.ecore;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.DocumentRoot;
import org.eclipse.bpmn2.impl.DefinitionsImpl;
import org.eclipse.bpmn2.impl.ProcessImpl;
import org.eclipse.bpmn2.impl.UserTaskImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.util.IResourceScopeCache;
import org.eclipse.xtext.util.PolymorphicDispatcher;
import org.eclipse.xtext.util.Tuples;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * @author Jan Koehnlein - Initial contribution and API
 */
public class BPMnQualifiedNameProvider extends
		IQualifiedNameProvider.AbstractImpl {

	public static final String DOCUMENT_ROOT = "BPMN";
	public static final String DEFINITIONS = "DEFINITIONS";
	public static final String PROCESS = "PROCESS";

	public static String getPerspectiveId(String processId, String userTaskId) {
		if ((processId == null) || (userTaskId == null)) {
			return null;
		} else {
			return processId + "_" + userTaskId;
		}
	}

	public static QualifiedName getPerspectiveId(ProcessImpl process,
			UserTaskImpl userTask) {
		String processId = id(process);
		String userTaskId = id(userTask);
		if ((processId == null) || (userTaskId == null)) {
			return null;
		} else {
			return QualifiedName
					.create(getPerspectiveId(processId, userTaskId));
		}
	}

	public static String id(ProcessImpl process) {
		return process == null ? null : beautify(process.getId());
	}

	public static String id(UserTaskImpl userTask) {
		return userTask == null ? null : beautify(userTask.getName());
	}

	private PolymorphicDispatcher<String> idDispatcher = new PolymorphicDispatcher<String>(
			"name", 1, 1, Collections.singletonList(this),
			PolymorphicDispatcher.NullErrorHandler.<String> get()) {
		@Override
		protected String handleNoSuchMethod(Object... params) {
			return null;
		}
	};

	@Inject
	private IResourceScopeCache cache = IResourceScopeCache.NullImpl.INSTANCE;

	public QualifiedName getFullyQualifiedName(final EObject obj) {
		return cache.get(Tuples.pair(obj, getCacheKey()), obj.eResource(),
				new Provider<QualifiedName>() {

					public QualifiedName get() {
						EObject temp = obj;
						String name = idDispatcher.invoke(temp);
						if (name == null) {
							return null;
						}
						QualifiedName qualifiedName = QualifiedName
								.create(StringUtils.strip(name, "."));
						if (!isRecurseParent(obj))
							return qualifiedName;
						QualifiedName parentsQualifiedName = getFullyQualifiedName(obj
								.eContainer());
						if (parentsQualifiedName == null)
							return null;
						else
							return parentsQualifiedName.append(qualifiedName);
					}

				});
	}

	protected boolean isRecurseParent(final EObject obj) {
		return obj.eContainer() != null && !(obj instanceof ProcessImpl);
	}

	protected String getCacheKey() {
		return "fqn";
	}

	protected String name(DefinitionsImpl element) {
		return DEFINITIONS;
	}

	protected String name(ProcessImpl element) {
		return id(element);
	}

	/**
	 * until a solution is found to edit the IDs of BPM user tasks inside the
	 * Eclipse Design Editor use the UserTask name to identify the user task!
	 * 
	 * @param element
	 * @return the <i>id</i> of the element - see description
	 */
	public String name(UserTaskImpl element) {
		return id(element);
	}

	protected String name(BaseElement element) {
		return element.getId();
	}

	private static String beautify(String name) {
		return name == null ? null : StringUtils.strip(
				StringUtils.stripAccents(
						name.replace("." + DEFINITIONS + "." + PROCESS,
								"." + PROCESS)
								.replace("." + DOCUMENT_ROOT + ".", ".")
								.replace("." + PROCESS, "").replace(" ", "")
								.replace("..", ".").replace("-", "_"))
						.replaceAll("\\W\\.", ""), ".");
	}

	public static QualifiedName beautity(QualifiedName name) {
		return QualifiedName.create(beautify(name.toString()).split("\\."));
	}

	protected String name(DocumentRoot element) {
		String uri = element.eResource().getURI().toString()
				.replace("/models", "").replace(".model", "");
		String[] tokens = uri.split("/");
		ProcessImpl p = ((ProcessImpl) (element.getDefinitions()
				.getRootElements().stream()
				.filter(e -> (e instanceof ProcessImpl)).findFirst()
				.orElse(null)));
		if (tokens.length > 2) {
			return p.getId() + "." + DOCUMENT_ROOT;
		} else {
			return "";
		}
	}
}
