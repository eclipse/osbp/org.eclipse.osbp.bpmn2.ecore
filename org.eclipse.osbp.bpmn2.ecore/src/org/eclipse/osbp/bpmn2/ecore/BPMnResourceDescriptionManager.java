/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 *///package org.eclipse.osbp.bpmn2.ecore;
//
//import org.eclipse.emf.ecore.resource.Resource;
//import org.eclipse.xtext.resource.IResourceDescription;
//import org.eclipse.xtext.resource.generic.GenericResourceDescriptionManager;
//import org.eclipse.xtext.resource.impl.DefaultResourceDescription;
//import org.eclipse.xtext.util.IResourceScopeCache;
//
//import com.google.inject.Inject;
//
//public class BPMnResourceDescriptionManager extends GenericResourceDescriptionManager {
//
//	@Inject
//	private BPMnResourceDescriptionStrategy resourceDescriptionStrategy;
//
//	@Inject
//	private IResourceScopeCache cache = new IResourceScopeCache.NullImpl();
//
//	public IResourceDescription getResourceDescription(Resource resource) {
//		return new DefaultResourceDescription(getProcess(resource), resourceDescriptionStrategy, cache);
//	}
//
//	private static Resource getProcess(Resource resource) {
//		return resource;
//	}
//	
//}
