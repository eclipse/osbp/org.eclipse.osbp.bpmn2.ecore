/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     Jan Koehnlein - Initial contribution and API
 *     Christophe Loetz (Loetz GmbH&Co.KG) - integration in and adaption to OSBP project                                                       
 *******************************************************************************/

package org.eclipse.osbp.bpmn2.ecore;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * @author Jan Koehnlein - Initial contribution and API
 */
public class NsURIQualifiedNameProvider extends BPMnQualifiedNameProvider {

//	@Override
//	protected String name(EPackage ePackage) {
//		return ePackage.getNsURI();
//	}

	@Override
	protected String getCacheKey() {
		return "nsURIfqn";
	}
	
	@Override
	protected boolean isRecurseParent(EObject obj) {
		return !(obj instanceof EPackage) && super.isRecurseParent(obj);
	}
}
